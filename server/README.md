# server

## Setup
```
npm install
```

### Run for development
```
npm run dev
```

### Run for production
```
npm start
```
