'use strict';

const dotenv = require('dotenv');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const router = require('./router');

const config = require('dotenv').config();
const { PORT = 3000} = config.parsed || {};

const app = express();
app.use(cors({}));  // TODO: move to config
app.use(morgan('dev'));
app.use(bodyParser.json());

app.use('/api', router);

app.listen(PORT, () => console.log(`Server listening on port ${PORT}.`));
