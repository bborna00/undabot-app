const express = require('express');
const yup = require('yup');

const router = express.Router();

const SUCCESS_MESSAGE = 'Your message has been sent!'

let contactFormSchema = yup.object().shape({
  email: yup.string().required().email(),
  message: yup.string().required().max(30),
});

router.post('/contact', async ({ body }, res) => {
  try {
    await contactFormSchema.validate(body);
    return res.status(200).json({ message: SUCCESS_MESSAGE })
  } catch ({ errors }) {
    return res.status(422).json({ errors })
  };
})

module.exports = router;
