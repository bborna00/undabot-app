/* eslint-disable sort-imports */
import {
  extend,
  localize,
  setInteractionMode,
  ValidationProvider,
  ValidationObserver
} from 'vee-validate';
import { email, max, required } from 'vee-validate/dist/rules';
import en from 'vee-validate/dist/locale/en.json';
import forEach from 'lodash/forEach';
import Vue from 'vue';

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

setInteractionMode('eager');

localize({ en });

forEach({ email, max, required }, (rule, name) => extend(name, rule));
