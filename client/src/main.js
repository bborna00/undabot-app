import '@babel/polyfill';
import '@mdi/font/css/materialdesignicons.css';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'vuetify/dist/vuetify.min.css';
import '@/utils/validation';

import App from './App.vue';
import Vue from 'vue';
import Vuetify from 'vuetify';
import router from './router';

Vue.config.productionTip = false;

Vue.use(Vuetify);

new Vue({
  router,
  vuetify: new Vuetify(),
  render: h => h(App)
}).$mount('#app');
