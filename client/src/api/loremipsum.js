import axios from 'axios';

const getLoremIpsumHtml = () => {
  return axios.get('loremipsum/10/medium/headers/ul/ol/dl/bq/code');
};

export default { getLoremIpsumHtml };
