import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:3000', // TODO: move to env
  headers: { 'Content-Type': 'application/json' }
});

const contact = data => {
  return api.post('/api/contact', data);
};

export default { contact };
