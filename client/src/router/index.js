import Vue from 'vue';
import VueRouter from 'vue-router';

/* eslint-disable sort-imports */
import AppHome from '@/components/home';
import AppContact from '@/components/contact';

Vue.use(VueRouter);

const routes = [{
  path: '/',
  name: 'home',
  component: AppHome
}, {
  path: '/contact',
  name: 'contact',
  component: AppContact
}];

export default new VueRouter({ routes });
