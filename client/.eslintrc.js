module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    semi: ['error', 'always'],
    'no-debugger': 'warn',
    'sort-imports': 'warn',
    'no-unused-vars': 'warn',
    'space-before-function-paren': ['error', 'never'],
    'vue/singleline-html-element-content-newline': ['off', 'always'],
    'vue/html-closing-bracket-newline': ['off', 'always'],
    'vue/max-attributes-per-line': ['off', 'always'],
    'vue/order-in-components': ['off', 'always']
  }
};
