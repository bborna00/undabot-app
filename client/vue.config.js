const TITLE = 'Undabot App';

module.exports = {
  chainWebpack: config => {
    config
    .devServer
    .proxy({ '^/loremipsum': { target: 'https://loripsum.net/api' } }); // TODO: Move to env

    config
      .plugin('html')
      .tap(([options]) => [{ ...options, title: TITLE }]);
  }
};
